
public class Queue {

	Object[] queue;
	private static final int TAMAÑO_INICIAL = 4;
	private int numElementos;

	public Queue() {
		queue = new Object[TAMAÑO_INICIAL];
		numElementos = 0;
	}

	public int size() {
		return numElementos;
	}

	public void offer(Object elemento) {
		if (numElementos == 0) {
			queue[0] = elemento;
			numElementos++;
		} else {
			comprobarLlenado();
			queue[numElementos] = elemento;
			numElementos++;
		}
	}

	private void comprobarLlenado() {
		// El array interno está casi lleno, se duplica el espacio.
		if (numElementos + 1 == queue.length) {
			Object[] arrayAmpliado = new Object[queue.length * 2];
			System.arraycopy(queue, 0, arrayAmpliado, 0, numElementos);
			queue = arrayAmpliado;
		}
	}

	public int indexOf(Object elem) {
		if (elem == null) {
			for (int i = 0; i < queue.length; i++) {
				if (queue[i] == null) {
					return i;
				}
			}
		} else {
			for (int i = 0; i < queue.length; i++) {
				if (elem.equals(queue[i])) {
					return i;
				}
			}
		}
		return -1;
	}

	public void clear() {
		queue = new Object[TAMAÑO_INICIAL];
		numElementos = 0;
	}

	public boolean contains(Object elem) {
		return indexOf(elem) != -1;
	}

	public Object peek() {
		// El índice debe ser válido para la lista.
		int indice = 0;
		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("Índice incorrecto: " + indice);
		}
		return queue[0];
	}

	public Object poll() {
		int indice = 0;
		// El índice debe ser válido para la lista.
		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("Índice incorrecto: " + indice);
		}
		// Elimina desplazando uno hacia la izquierda, sobre la posición a borrar.
		Object elem = queue[indice];
		System.arraycopy(queue, indice + 1, queue, indice, numElementos - (indice + 1));

		// Ajusta el último elemento.
		queue[numElementos - 1] = null;
		numElementos--;
		return elem;
	}

//	public boolean isEmpty() {
//		if (pila == null) {
//			return true;
//		}
//		return false;
//
//	}
}
