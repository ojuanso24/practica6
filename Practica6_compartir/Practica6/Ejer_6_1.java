

public class Ejer_6_1 {
	public static void main(String[] args) {
		ListaArray listaCompra = new ListaArray();
		listaCompra.add("Rojo");
		listaCompra.add("Azul");
		listaCompra.add("Verde");
		listaCompra.remove("Azul");
		listaCompra.add(1, "Gris");
		listaCompra.add(0, "Negro");

		System.out.format("Los %d elementos de la lista de la compra son:\n", listaCompra.size());
		for (int i = 0; i < listaCompra.size(); i++) {
			System.out.format("%s\n", listaCompra.get(i));
		}
		System.out.format("¿Hay Rojo en la lista? %b", listaCompra.contains("Rojo"));
	}

}
