
public class Ejer_6_6 {
	public static void main(String[] args) {
		Pila pila = new Pila();
		pila.push("1");
		pila.push("2");
		pila.push("3");

		
		System.out.format("%s\n", pila.peek());
		pila.pop();
		System.out.format("%s\n", pila.peek());
		pila.pop();
		System.out.format("%s\n", pila.peek());
		pila.pop();


		pila.push("4");
		pila.push("5");
		pila.pop();

		pila.push("6");
		pila.push("7");

		System.out.format("%s\n", pila.peek());
		pila.pop();
		System.out.format("%s\n", pila.peek());
		pila.pop();
		System.out.format("%s\n", pila.peek());
		pila.pop();

		System.out.println(pila.contains("6"));
	}
}
