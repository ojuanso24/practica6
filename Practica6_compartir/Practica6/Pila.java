
public class Pila {

	Object[] pila;
	private static final int TAMAÑO_INICIAL = 4;
	private int numElementos;

	public Pila() {
		pila = new Object[TAMAÑO_INICIAL];
		numElementos = 0;
	}

	public int size() {
		return numElementos;
	}

	public void push(Object elemento) {
		if (numElementos == 0) {
			pila[0] = elemento;
			numElementos++;
		} else {
			comprobarLlenado();
			pila[numElementos] = elemento;
			numElementos++;
		}
	}

	private void comprobarLlenado() {
		// El array interno está casi lleno, se duplica el espacio.
		if (numElementos + 1 == pila.length) {
			Object[] arrayAmpliado = new Object[pila.length * 2];
			System.arraycopy(pila, 0, arrayAmpliado, 0, numElementos);
			pila = arrayAmpliado;
		}
	}

	public int indexOf(Object elem) {
		if (elem == null) {
			for (int i = 0; i < pila.length; i++) {
				if (pila[i] == null) {
					return i;
				}
			}
		} else {
			for (int i = 0; i < pila.length; i++) {
				if (elem.equals(pila[i])) {
					return i;
				}
			}
		}
		return -1;
	}

	public void clear() {
		pila = new Object[TAMAÑO_INICIAL];
		numElementos = 0;
	}

	public boolean contains(Object elem) {
		return indexOf(elem) != -1;
	}

	public Object peek() {
		// El índice debe ser válido para la lista.
		int indice = 0;
		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("Índice incorrecto: " + indice);
		}
		return pila[size() - 1];
	}

	public Object pop() {
		int indice = size() - 1;
		// El índice debe ser válido para la lista.
		if (indice >= numElementos || indice < 0) {
			throw new IndexOutOfBoundsException("Índice incorrecto: " + indice);
		}
		// Elimina desplazando uno hacia la izquierda, sobre la posición a borrar.
		Object elem = pila[indice];
		System.arraycopy(pila, indice + 1, pila, indice, numElementos - (indice + 1));

		// Ajusta el último elemento.
		pila[numElementos - 1] = null;
		numElementos--;
		return elem;
	}

//	public boolean isEmpty() {
//		if (queue != null) {
//			return true;
//		}
//		return false;
//
//	}
}
