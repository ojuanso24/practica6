
public class Ejer_6_7 {
	public static void main(String[] args) {
		Queue cola = new Queue();

		cola.offer("1");
		cola.offer("2");
		cola.offer("3");
		cola.offer("4");
		cola.offer("5");
		cola.offer("6");
		cola.offer("7");
		cola.offer("8");
		cola.offer("9");
		cola.offer("10");
		cola.offer("11");
		cola.offer("12");
		cola.offer("13");
		cola.offer("14");
		cola.offer("15");

		System.out.println();

		System.out.format("Los %d elementos de la lista de la compra son:\n", cola.size());
		for (int i = 0; i < cola.size();) {
			System.out.format("%s\n", cola.peek());
			cola.poll();
		}


//		System.out.format("%s\n", cola.get());
//		cola.remove();
//		System.out.format("%s\n", cola.get());
//		cola.remove();
//		System.out.format("%s\n", cola.get());
//		cola.remove();
//		System.out.format("%s\n", cola.get());
//		cola.remove();
//		System.out.format("%s\n", cola.get());
//		cola.remove();
//		System.out.format("%s\n", cola.get());
	}
}
